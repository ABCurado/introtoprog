# Results of the 2015 Election in relation to socio-economic factors of Portuguese municipalities

__Steps performed during the analysis:__  

* Data extraction
* Data import and cleaning
* Understanding election results
* Understanding socio-economic factors
* Visualizing correlations
* Regression to find correlations

All the work can can be visualized through the notebooks and the present report